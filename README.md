# README #

Multi-region conjugate heat transfer solver for thermal energy transport between two separate regions (fluid and solid) supporting Dirichlet-Neumann monolithic and partitioned coupling methods.
    
Additionally, the solver permits a thermal contact resistance between regions via a "resistive" interface boundary condition.

### What is this repository for? ###

* Training material for the 15th International OpenFOAM Workshop at Virginia Tech, June 22 - 25, 2020
* Building on the training provided by Brent A. Craven and Robert L. Campbell at the 6th International OpenFOAM Workshop at Penn State University
* Version 0.2 (added monolithic solution capabilities in a unified framework)

### How do I get set up? ###

* Compilation with FOAM-extend v4.0 and v4.1
* Execute wmake in applications/solvers/conjugateHeatFoam/

### Who do I talk to? ###

* Holger Marschall <holger.marschall@tu-darmstadt.de>
